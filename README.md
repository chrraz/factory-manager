## What it is
Factory Manager is an instance manager for [Factorio][factorio] written in [Node.js][node].
It is inspired by narc0tiq's [Factorio Update Helper][narc0tic-repo] but seeks to be a full-featured, easy-to-use instance manager instead of "just" an updater.

## What it does
What you are looking at here is simply a placeholder version with no actual functionality.

Features will be added down the line so keep an eye on the [issue tracker][issues] and of course the repo itself.

## How you install it
The `master` branch of this repo will contain stable releases that should be mostly bug-free while the `develop` branch contains more up-to-date but unstable versions.

You can either download this repo as a [zip][repo-download] or `git clone` it.
By cloning it you can also update Factory Manager by executing `git pull`.

Because Factory Manager is written in Node.js you need to have Node installed on your computer as well.
The two main ways of doing so is from [the official download page][node-download] or with [nvm][nvm].
It is also available in most package managers.

Once the code is downloaded, you install its dependencies with `npm install` (or `npm i` if you don't want to type as much).

Factory manager will be developed according to the active LTS release at the time.
This is the default download from [the official download page][node-download].

### Long story short
1. Make sure that you have Node.js installed
	* Can be installed from [the official download page][node-download] or with [nvm][nvm]
	* Use the latest LTS version
1. Download this repo
	* You can download it as a [zip][repo-download] or `git clone` it
1. Install the dependencies
	* Run `npm install` or `npm i` (they do the same thing)

## How you use it
In the root directory there will be a file called `fm`. It is the entry point to all the subcommands offered by Factory Manager.

`fm --help` will give you a list of all the available subcommands and how to use them, but the gist of it is that they are all called by executing `fm <command> [arguments...]`.

The commands will also be listed under [What it does][section-what-it-does]

## Legal stuff
This project is is released under the [BSD 2-Clause License][osi-bsd-2]. A copy of the license can be found in the file called LICENSE.

narc0tiq's [Factorio Update Helper][narc0tic-repo] is released under the MIT License. A copy of the license can be found in the file called LICENSE-factorio-update-helper.

Node.js is a trademark of Joyent, Inc. and is used with its permission. I am not endorsed by or affiliated with Joyent.

Factorio is owned and under copyright by Wube Software.

[node]: https://nodejs.org/ "Node.js homepage"
[node-download]: https://nodejs.org/en/download/ "Node.js download page"
[factorio]: http://www.factorio.com/ "Factorio homepage"
[issues]: https://bitbucket.org/chrraz/factory-manager/issues?status=open "Issues"
[narc0tic-repo]: https://github.com/narc0tiq/factorio-updater "narc0tic's factorio-updater repo"
[nvm]: http://nvm.sh/ "nvm homepage"
[repo-download]: https://bitbucket.org/chrraz/factory-manager/downloads "Project download page"
[section-what-it-does]: #markdown-header-what-it-does
[osi-bsd-2]: https://opensource.org/licenses/BSD-2-Clause "The BSD 2-Clause License"
